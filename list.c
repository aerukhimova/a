#include "list.h"
#include <stdio.h>
#include <stdlib.h>


//создает новый элемент
node *list_create(int data, node *next) {
    node *new_node = (node *) malloc(sizeof(node));
    if (new_node == NULL) {
        printf("Error creating a new node.\n");
        exit(0);
    }
    new_node->data = data;
    new_node->next = next;

    return new_node;
}

//добавляет элемент в начало
node *list_add_front(node *pointer, int data) {
    node *new_node = list_create(data, pointer);
    pointer = new_node;
    return pointer;
}

//добавляет элемент в конец
node *list_add_back(node *pointer, int data) {
    if (pointer == NULL)
        return list_create(data, pointer);

    node *iter = pointer;
    while (iter->next != NULL)
        iter = iter->next;


    node *new_node = list_create(data, NULL);
    iter->next = new_node;

    return pointer;
}

//выводит лист
void list_show(node *i) {
    if (i == NULL) {
        printf("List is empty");
    }
    while (i != NULL) {
        printf("%d ", i->data);
        i = i->next;
    }
}


node *list_free(node *nod) {
    node *nodeForFree;
    while (nod != NULL) {
        nodeForFree = nod;
        nod = nod->next;
        free(nodeForFree);
    }
    return nod;
}


//подсчет количества элемнетов
int list_length(node *pointer) {
    node *iter = pointer;
    int count = 0;
    while (iter != NULL) {
        count++;
        iter = iter->next;
    }
    return count;
}

//сумма элементов
int list_sum(node *i) {
    int sum = 0;
    while (i != NULL) {
        sum += i->data;
        i = i->next;
    }
    return sum;
}

//достаёт значение элемента по индексу
node *list_get(node *pointer, int index) {
    int i = 0;
    if (list_length(pointer) <= index)
        return NULL;
    node *iter = pointer;
    while (i < index) {
        iter = iter->next;
        i++;
    }
    return iter;
}

void list_set(node *pointer, int index, int data) {
    int i = 0;
    if (list_length(pointer) <= index)
        return;
    node *iter = pointer;
    while (i < index) {
        iter = iter->next;
        i++;
    }
    iter->data = data;
}


void foreach(node *pointer, void (*func)(int)) {
    size_t i;
    size_t length = list_length(pointer);
    for (i = 0; i < length; i++) {
        func(list_get(pointer, i)->data);
    }
}

node *map(int (*func)(int), node *pointer) {
    size_t i;
    size_t length = list_length(pointer);
    node *new_node = NULL;
    new_node = list_create(func(list_get(pointer, 0)->data), new_node);
    for (i = 1; i < length; i++) {
        list_add_back(new_node, func(list_get(pointer, i)->data));
    }
    return new_node;
}

void map_mut(int (*func)(int), node *pointer) {
    size_t i;
    size_t length = list_length(pointer);
    for (i = 0; i < length; i++) {
        list_set(pointer, i, func(list_get(pointer, i)->data));
    }
}

int foldl(int rax, int (*func)(int, int), node *start) {
    size_t i;
    size_t length = list_length(start);
    for (i = 0; i < length; i++) {
        rax = func(rax, list_get(start, i)->data);
    }
    return rax;
}

node *iterate(int s, int n, int (*func)(int)) {
    size_t i;
    node *new_pointer = NULL;

    new_pointer = list_create(s, new_pointer);
    for (i = 1; i < n; i++) {
        s = func(s);
        new_pointer = list_add_back(new_pointer, s);
    }
    return new_pointer;
}

bool save(node *pointer, const char *filename) {
    int len = list_length(pointer);
    int i;
    int has_error;

    FILE *file = fopen(filename, "w");
    if (!file) return false;

    for (i = 0; i < len; i++) fprintf(file, "%d ", list_get(pointer, i)->data);

    has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool load(node **pointer, const char *filename) {
    int res;
    int has_error;

    FILE *file = fopen(filename, "r");
    if (!file) return false;
    while (fscanf(file, "%d ", &res) == 1) {
        *pointer = list_add_back(*pointer, res);
    }
    has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool serialize(node *pointer, const char *filename) {
    int len = list_length(pointer);
    int i;
    int has_error;
    FILE *file = fopen(filename, "wb");
    if (!file) return false;
    for (i = 0; i < len; i++)
        fwrite(&list_get(pointer, i)->data, sizeof(list_get(pointer, i)->data), 1, file);

    has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool deserialize(node **pointer, const char *filename) {
    int res;
    int has_error;

    FILE *file = fopen(filename, "rb");
    if (!file) return false;
    while (fread(&res, sizeof(res), 1, (FILE *) file) == 1) {
        *pointer = list_add_back(*pointer, res);
    }
    has_error = ferror(file);
    fclose(file);
    return !has_error;
}




