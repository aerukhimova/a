default:
	gcc -c main.c list.c functions.c -pedantic -Wall -Werror
	gcc -o main main.o list.o functions.o -lm
clean:
	rm -rf main *.o
