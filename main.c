#include <stdio.h>
#include "list.h"
#include "functions.h"

void test(node *pointer) {
    node *tmp;
    if (list_length(pointer) > 0) {
        printf("Demonstration of foreach\n");
        printf("separated by spaces\n");
        foreach(pointer, print_value); //через пробел
        printf("\n");
        printf("number per line\n");
        foreach(pointer, print_value_ln);//на каждой строке
        printf("\n");

        printf("Demonstration of map\n");
        printf("squares\n");
        tmp = map(square, pointer);
        list_show(tmp);
        printf("\n");

        printf("cubes\n");
        tmp = map(cube, pointer);
        list_show(tmp);
        printf("\n\n");

        printf("Demonstration of map_mut\n");
        printf("modules\n");
        map_mut(abs, pointer);
        list_show(pointer);
        printf("\n\n");

        //sum, min, max
        printf("Demonstration of foldl\n");
        printf("Sum: %d\n", foldl(0, sum, pointer));
        printf("Min: %d\n", foldl(list_get(pointer, 0)->data, min, pointer));
        printf("Max: %d\n", foldl(list_get(pointer, 0)->data, max, pointer));
        printf("\n");



        printf("Demonstration of save\n");
        if (save(pointer, "filename1.txt")) {
            printf("true\n");
        } else printf("false\n");
        printf("\n");

        node* array = NULL;
        printf("Demonstration of load\n");
        if (load(&array, "filename1.txt")) {
            printf("true\n");
            list_show(array);
        } else printf("false\n");
        printf("\n\n");


        printf("Demonstration of serialize\n");
        if (serialize(pointer, "filename1.bin")) {
            printf("true\n");
        } else printf("false\n");
        printf("\n");

        array = NULL;
        printf("Demonstration of deserialize\n");
        if (deserialize(&array, "filename1.bin")) {
            printf("true\n");
            list_show(array);
        } else printf("false\n");
        printf("\n\n");




    }
    printf("Demonstration of iterate\n");
    printf("list of the powers of two\n");
    tmp = NULL;
    tmp = iterate(1, 10, mul2);
    list_show(tmp);
    printf("\n");

}

void hello() {
    printf("Hello\n");
    printf("0 hello\n");
    printf("1 add to front\n");
    printf("2 add to back\n");
    printf("3 search\n");
    printf("4 sum\n");
    printf("5 length\n");
    printf("6 clear\n");
    printf("7 show\n");
    printf("8 exit\n");
    printf("9 test\n");

}

int main(int argc, char **argv) {
    int command = 0;
    int data;

    node *pointer = NULL;

    int sum = 0;
    int length = 0;

    hello();
    while (1) {
        printf("\nWrite a command(0-6):");
        scanf("%d", &command);

        switch (command) {
            case 0:
                hello();
                break;
            case 1:
                printf("Please write a number to add:");
                scanf("%d", &data);
                pointer = list_add_front(pointer, data);
                list_show(pointer);
                break;
            case 2:
                printf("Please write a number to add:");
                scanf("%d", &data);
                pointer = list_add_back(pointer, data);
                list_show(pointer);
                break;
            case 3:
                printf("Please write an index to search:");
                scanf("%d", &data);
                node *tmp = list_get(pointer, data);
                if (tmp != NULL) {
                    printf("Element with index %d: %d.", data, tmp->data);
                } else {
                    printf("List too short");
                }
                break;
            case 4:
                sum = list_sum(pointer);
                printf("Sum of all elements: %d", sum);
                break;
            case 5:
                length = list_length(pointer);
                printf("Length of list: %d", length);
                break;
            case 8:
                printf("Have a good day\n");
                printf("Goodbye\n");
                list_free(pointer);
                return 0;
            case 7:
                list_show(pointer);
                break;
            case 9:
                test(pointer);
                break;
            case 6:
                pointer = list_free(pointer);

        }

    }

}