#include "functions.h"

void print_value(int x) {
    printf("%d ", x);
}

void print_value_ln(int x) {
    printf("%d\n", x);
}

int square(int x) {
    if (x * x > INT_MAX) return 0;
    return x * x;
}

int cube(int x) {
    if (x * x * x > INT_MAX || x * x * x < INT_MIN) return 0;
    return x * x * x;
}

int sum(int a, int b) {
    if ((a + b) > INT_MAX || (a + b) < INT_MIN) return 0;
    return a + b;
}

int max(int a, int b) {
    if (a >= b) return a;
    return b;
}

int min(int a, int b) {
    if (b <= a) return b;
    return a;
}

int abs(int a);

int mul2(int a) {
    if (a * 2 > INT_MAX || a * 2 < INT_MIN) return 0;
    return a * 2;
}
